# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from .forms import CategoryForm, PageForm, UserProfileForm
from django.shortcuts import render, redirect, HttpResponse
from .models import Category, Page, UserProfile, User
from django.contrib.auth.decorators import login_required
from datetime import datetime

def index(request):

    category_list = Category.objects.order_by('-likes')[:5]
    popular_pages = Page.objects.order_by('-views')[:5]

    context_dict = {'categories': category_list, 'popular_pages': popular_pages}

    visits = request.session.get('visits')
    if not visits:
        visits = 1
    reset_last_visit_time = False

    last_visit = request.session.get('last_visit')
    if last_visit:
        last_visit_time = datetime.strptime(last_visit[:-7], "%Y-%m-%d %H:%M:%S")

        if (datetime.now() - last_visit_time).seconds > 0:
            # ...прибавляем единицу к предыдущему значению cookie...
            visits = visits + 1
            # ...и обновляем cookie last visit cookie.
            reset_last_visit_time = True
    else:
        # Cookie last_visit не существует, поэтому создаём его для текущей даты/времени.
        reset_last_visit_time = True

    if reset_last_visit_time:
        request.session['last_visit'] = str(datetime.now())
        request.session['visits'] = visits
    context_dict['visits'] = visits

    response = render(request,'rango/index.html', context_dict)

    return response

def about(request):
    return render(request, 'rango/about.html')

def category(requset, category_name_slug):
    context_dict = {}

    try:
        category = Category.objects.get(slug=category_name_slug)
        context_dict['category_name'] = category.name
        pages = Page.objects.filter(category=category)
        context_dict['pages'] = pages
        context_dict['category'] = category
        context_dict['category_name_slug'] = category_name_slug
    except Category.DoesNotExist:
        pass

    return render(requset, 'rango/category.html', context_dict)

def add_category(request):
    if request.method == 'POST':
        form = CategoryForm(request.POST)
        if form.is_valid():
            form.save(commit=True)
            return index(request)
        else:
            return print(form.errors)
    else:
        form = CategoryForm()
    return render(request, 'rango/add_category.html', {'form':form})

def add_page(request, category_name_slug):

    try:
        cat = Category.objects.get(slug=category_name_slug)
    except Category.DoesNotExist:
        cat = None

    if request.method == 'POST':
        form = PageForm(request.POST)
        if form.is_valid():
            if cat:
                page = form.save(commit=False)
                page.category = cat
                page.views = 0
                page.save()
                return redirect('/rango/')
        else:
            print (form.errors)
    else: # GET запрос на вывод формы
        form = PageForm()

    context_dict = {'form':form, 'category': cat}

    return render(request, 'rango/add_page.html', context_dict)

# Декоратор login_required() гарантирует, что только авторизированные пользователи смогут получить доступ к этому представлению.
@login_required
def restricted(request):

    text = "Since you're logged in, you can see this text!"
    return render(request, 'rango/restricted.html', {'text': text})

def track_url(request):

    if request.method == 'GET':
        if 'page_id' in request.GET:
            page_id = request.GET['page_id']
            try:
                page = Page.objects.get(id = page_id)
                page.views += 1
                page.save()
                return redirect(page.url)
            except Page.DoesNotExist:
                return redirect('/rango/')

        else:
            return redirect('/rango/')
    else:
        render(request, 'rango/index.html')

def register_profile(request):

    if request.method == 'POST':
        form = UserProfileForm(request.POST)
        user = User.objects.get(id = request.user.id)

        if form.is_valid():
            user_profile = UserProfile(user = user)
            website = request.POST['website']
            user_logo = request.FILES['picture']
            user_profile.website = website
            user_profile.picture = user_logo
            user_profile.save()
            return redirect('/rango/')
        else:
            render(request, 'registration/profile_registration.html')

    else:
        form = UserProfileForm()

    return render(request, 'registration/profile_registration.html', {'form':form})

@login_required
def like_category(request):

    cat_id = None
    if request.method == 'GET':
        cat_id = request.GET['category_id']

    likes = 0
    if cat_id:
        cat = Category.objects.get(id=int(cat_id))
        if cat:
            likes = cat.likes + 1
            cat.likes = likes
            cat.save()
    return HttpResponse(likes)